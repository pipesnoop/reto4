package co.com.sofka;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

public class PokemonSetup extends GeneralSetup{
    protected static final String URL_BASE_POKEMON = "https://pokeapi.co";
    protected static final String RESOURCE_POKEMON = "/api/v2/pokemon?limit=10&offset=0";

    protected void setupPokemon() {
        actorCan(URL_BASE_POKEMON);
    }

}
